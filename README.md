### This is a simple plugin whose main task is displaying content with GDPR consent. Written in vanilla JS.


###### SETUP

1. Download plugin.js and pstyle.css files
2. Add the following to your HTML file:
```html
 <script async src="Path/To/plugin.js" type="text/javascript"></script>
 <link rel="stylesheet" href="Path/To/pstyle.css"  />
```
###### WARNING!
It's important to add `async` attribute to your `<script>`  tag  before  `src`  attribute. In this way, no matter where you'll include your `<script>` (in  the  `<head>`  section
or at the bottom of the `<body>)` plugin will always appear ASAP.

###### SIDENOTe about styling:
The base functionality of the plugin is covered with CSS styling
by using id `#consent` and class `.consent_only`.
For easy manipulating them I highly recommend you to use SASS :)


