//Wrapping all in IIFE to let plugin run asap
(() => {

    if (document.cookie.split(';').filter(item => {
        return item.indexOf('consent=') >= 0
    }).length) {
        document.getElementById('consent').style.display = "none";
    };


    this.pattern = '<div id="consent" class="consent_only">' +
        '<h2>GDPR Consent</h2>' +
        '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt </p>' +
        '<button type="button" id="accept">Accept</button>' + '<button type="button" id="cancel">Cancel</button>' +
        '</div>';

    // Add it to the body on init
    document.body.innerHTML += this.pattern;

    // Turning off scrolling 
    window.addEventListener('load', () => {
        document.documentElement.style.overflow = 'hidden';  // firefox, chrome
        document.body.scroll = "no"; // ie only
    });


    // Reload scrollbar and closing plugin
    document.querySelectorAll('button').forEach(btn => {
        btn.addEventListener('click', event => {
            document.documentElement.style.overflow = 'auto';  // firefox, chrome
            document.body.scroll = "yes"; // ie only*/
            document.getElementById('consent').style.display = "none";

            //Set cookies 
            if (event.target.id === "accept") {
                document.cookie = 'consent=Accept; max-age=86400'
            } else if (event.target.id === "cancel") {
                document.cookie = 'consent=Cancel; max-age=86400'
            }

        });
    });


})();